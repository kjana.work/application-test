import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class AddEditComponent implements OnInit, OnChanges {

  @Input() selectedScope:any

  @Input() editData:any ={}

  @Output() unsetscope = new EventEmitter<string>();

  constructor(
    private toastrService: ToastrService,
    private httpservice: HttpService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.selectedScope == 'Edit'){
      this.rowData.name = this.editData.name
      this.rowData.state = this.editData.state
      this.rowData.zip = this.editData.zip
      this.rowData.amount = this.editData.amount
      this.rowData.qty = this.editData.qty
      this.rowData.item = this.editData.item
    }
  }

  rowData:any = {}

  onSubmit(){

    if(this.selectedScope == 'Edit') {
      let params = new URLSearchParams();
    params.set('method', 'edit');
    params.set('id', this.editData.id);

    this.httpservice.editrow(params,this.rowData).subscribe(data=> {
      this.toastrService.success('Edited Successfully!', '', {
        timeOut: 2000,
      });
      this.unsetscope.emit()
    })

    }
    if(this.selectedScope == 'Add') {
      this.httpservice.addrow(this.rowData).subscribe(data=> {
        this.toastrService.success('Added Successfully!', '', {
          timeOut: 2000,
        });
        this.unsetscope.emit()
      })
    }
  }

  ngOnInit(): void {
  }

}
