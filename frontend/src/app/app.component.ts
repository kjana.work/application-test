import { Component, OnInit } from '@angular/core';
import { HttpService } from './services/http.service';
import { LoaderService } from './services/loader.service';
import { ToastrService } from 'ngx-toastr';
declare var window: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit{
  title = 'frontend';
  showLoader = true

  selectedScope = ''

  selectedrowID = ''
  selectedlowData:any = {}
  deleteModal:any

  addEditCanvas:any

  rowlist :any = []
  constructor(
    private httpservice : HttpService,
    private loaderservice: LoaderService,
    private toastrService: ToastrService
  ) {
    this.loaderservice.loaderStatus().subscribe(status => {
      setTimeout(() => {
        this.showLoader = status;
      }, 0)
    });
  } 

  ngOnInit(): void {
    this.getupdatedList()
    this.addEditCanvas = new window.bootstrap.Offcanvas(
      document.getElementById('addeditoffcanvasRight')
    );
    this.deleteModal = new window.bootstrap.Modal(
      document.getElementById('deleteModal')
    );
  }


  editbyRow(rowData:any) {
    this.selectedlowData = rowData[1]
    this.selectedScope = 'Edit'
  }

  deletebyID(id:any) {
    this.selectedrowID = id
  }

  deleterow() {
    let params = new URLSearchParams();
    params.set('method', 'delete');
    params.set('id', this.selectedrowID);

    this.httpservice.editrow(params,{}).subscribe(data=> {
      this.toastrService.success('Deleted Successfully!', '', {
        timeOut: 2000,
      });
      this.unsetscope()
    })

  }

  newrowAdd() {
    this.selectedScope = 'Add'
  }

  unsetscope() {
    this.addEditCanvas.hide()
    this.deleteModal.hide()
    this.selectedScope = ''
    this.selectedrowID = ''
    this.selectedlowData = {}
    this.getupdatedList()
  }

  

  getupdatedList() {
    this.httpservice.getLIST().subscribe(data=> {
      this.rowlist = Object.keys(data).map((key) => [key, data[key]])
    })
  }
  
}
