import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, catchError, finalize, throwError } from 'rxjs';
import { LoaderService } from '../services/loader.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class HttpInterseptorInterceptor implements HttpInterceptor {

  constructor(
    private loaderservice: LoaderService,
    private toastrService: ToastrService
  ) {}


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loaderservice.show()
    return next.handle(req).pipe(
      catchError((error: any) => {
        if(error.status != 200) {
        let errorMessage = '';

        if (error.error.msg ) {
          errorMessage = error.error.msg
        } else {
          errorMessage = `Oops something went wrong!! Please try after sometime.`;
        }

        this.toastrService.error(errorMessage, '', {
          timeOut: 2000,
        });
        return throwError(errorMessage);
        }
        return throwError(null);
      }),

      finalize(() => this.loaderservice.hide())
    );
  }

   
}
