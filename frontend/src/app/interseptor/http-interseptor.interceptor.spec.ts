import { TestBed } from '@angular/core/testing';

import { HttpInterseptorInterceptor } from './http-interseptor.interceptor';

describe('HttpInterseptorInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HttpInterseptorInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: HttpInterseptorInterceptor = TestBed.inject(HttpInterseptorInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
