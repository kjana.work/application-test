import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_CONFIG } from '../config/config.const';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  public headers: any;

  constructor(
    private http: HttpClient
  ) { }
  public prepareHeader() {
    this.headers = {
      headers: new HttpHeaders({
      
      })
    };
  }

  public getLIST(): Observable<any> {
    let url = API_CONFIG.DATA_LOCATION
    this.prepareHeader();
    return this.http.get(url, this.headers);
  }


  public addrow( data?: any): Observable<any> {
    let url = API_CONFIG.DATA_LOCATION
    this.prepareHeader();
    return this.http.post(url, data, this.headers);
  }


  public editrow(query: any, data?: any): Observable<any> {
    let url = API_CONFIG.DATA_LOCATION + query.toString();
    this.prepareHeader();
    return this.http.put(url, data, this.headers);
  }
}
