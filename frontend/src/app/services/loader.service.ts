import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  constructor() {}

  private showLoader = new BehaviorSubject<boolean>(false);
  showIndex = 0;

  show() {
    this.showIndex++;
    if (this.showIndex > 0) {
      this.showLoader.next(true);
    }
  }

  loaderStatus(): Observable<any> {
    return this.showLoader.asObservable();
  }

  hide() {
    if (this.showIndex > 0) {
      this.showIndex--;
    }
    if (this.showIndex == 0) {
      this.showLoader.next(false);
    }
  }

}
