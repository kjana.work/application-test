

<?php

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, PUT');
    header('Access-Control-Allow-Headers: token, Content-Type');
    header('Access-Control-Max-Age: 1728000');
    header('Content-Length: 0');
    header('Content-Type: text/plain');
    die();
}

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$file="data.csv";

$csv= file_get_contents($file);
$lines = file($file, FILE_IGNORE_NEW_LINES);

$csvarray = array();
foreach ($lines as $x) {
    array_push($csvarray,explode(',', $x));
}

if($_SERVER['REQUEST_METHOD'] === 'GET') {

    $json = rewrap(array_slice($csvarray, 0, sizeof($csvarray)));
    $res = array_filter($json, function ($var) {
        return ($var['deleted'] != '1');
    });

    print_r(json_encode($res));
    exit;
}

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    $data = json_decode(file_get_contents('php://input'), true);

    $newID = sizeof($csvarray);
	$name = getdataFromObject($data,'name');
    $state = getdataFromObject($data,'state');
    $zip = getdataFromObject($data,'zip');
    $amount = getdataFromObject($data,'amount');
    $qty = getdataFromObject($data,'qty');
    $item = getdataFromObject($data,'item');
    $status = '0';

    $lines[$newID] = "{$newID},{$name},{$state},{$zip},{$amount},{$qty},{$item},{$status}";

    $A_data_string = implode("\n",$lines);

    $f = fopen($file,'w');
    fwrite($f,$A_data_string);
    fclose($f);

    $object = new stdClass();
    $object->status = 1;
    $object->msg = 'Successfully Added';

    print_r(json_encode($object));
    exit;
}

if($_SERVER['REQUEST_METHOD'] === 'PUT' && $_GET['method'] == 'edit') {
    $row = $_GET['id'];

    $data = json_decode(file_get_contents('php://input'), true);
   
	$name = getdataFromObject($data,'name');
    $state = getdataFromObject($data,'state');
    $zip = getdataFromObject($data,'zip');
    $amount = getdataFromObject($data,'amount');
    $qty = getdataFromObject($data,'qty');
    $item = getdataFromObject($data,'item');

    
    if($name == 'null'){
        $name = $csvarray[$row][1];
    }
    if($state == 'null'){
        $state = $csvarray[$row][2];
    }
    if($zip == 'null'){
        $zip = $csvarray[$row][3];
    }
    if($amount == 'null'){
        $amount = $csvarray[$row][4];
    }
    if($qty == 'null'){
        $qty = $csvarray[$row][5];
    }
    if($item == 'null'){
        $item = $csvarray[$row][6];
    }
    $status = $csvarray[$row][7];


    $lines[$row] = "{$row},{$name},{$state},{$zip},{$amount},{$qty},{$item},{$status}";

    $E_data_string = implode("\n",$lines);

    $f = fopen($file,'w');
    fwrite($f,$E_data_string);
    fclose($f);

    $object = new stdClass();
    $object->status = 1;
    $object->id = $row;
    $object->msg = 'Edited Row';
    print_r(json_encode($object));
    exit;
}

if($_SERVER['REQUEST_METHOD'] === 'PUT' && $_GET['method'] == 'delete') {
    $row = $_GET['id'];

    $lines[$row] = "{$csvarray[$row][0]},{$csvarray[$row][1]},{$csvarray[$row][2]},{$csvarray[$row][3]},{$csvarray[$row][4]},{$csvarray[$row][5]},{$csvarray[$row][6]},1";

    $data_string = implode("\n",$lines);

    $f = fopen($file,'w');
    fwrite($f,$data_string);
    fclose($f);

    $object = new stdClass();
    $object->status = 1;
    $object->id = $row;
    $object->msg = 'Successfully Deleted';
    print_r(json_encode($object));
    exit;
}


function rewrap(Array $input){
    $key_names = array_shift($input);
    $output = Array();
    foreach($input as $index => $inner_array){
        $output[] = array_combine($key_names,$inner_array);
    }
    return $output;
}

function getdataFromObject($data, $key) {
    if(array_key_exists($key,$data)){
        return $data[$key];
    } else {
        return 'null';
    }
}

exit;

?>
